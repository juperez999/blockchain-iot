import sys
import json
import time
import logging
import threading
import block_args as args
from listening import main_listen_loop, first_man, join_up, proc_all, sync_up
from flask import Flask, request
from state_machine import ZMQ_Soc, BlockQueue
from blockchain import blockChain
from argparse import ArgumentParser
import requests
import string
import random

logFormatter = logging.Formatter('%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} - %(message)s','%m-%d %H:%M:%S')
log = logging.getLogger()
fileHandler = logging.FileHandler("{}.log".format('blockchain'))
fileHandler.setFormatter(logFormatter)
log.addHandler(fileHandler)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
log.addHandler(consoleHandler)
log.setLevel(logging.INFO)


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('--node-ip', default=None)
    return parser.parse_args()


def broadcast_block(sub_filter, payload):
    # zmq_obj = ZMQ_Soc.get_instance()
    bq = BlockQueue.get_instance()
    payload = [f'{sub_filter}', f'{payload}']
    bq.add_block(payload)
    # zmq_obj.broadcast(str(gen_block))


def reg_api(con_str, topic, p_key, sub_filter='hello'):
    time.sleep(2)
    broadcast_block('register',
                    f'{sub_filter}|_|{topic}_|_{con_str}_|_{p_key}')
    return True


def oracle_api(payload, iam=False):
    if not i_am_oracle():
        return False
    req = f'oracle is|_|{payload}'
    if iam:
        req = f'I am oracle|_|{payload}'
    broadcast_block('oracle', req)
    return True


def i_am_oracle():
    bc = blockChain.get_instance()
    return bc.current_oracle == args.my_topic


app = Flask(__name__)
app.debug = False


@app.route('/blocks', methods=['POST'])
def blocks():
    return str(blockChain.get_instance().blocks)


@app.route('/join', methods=['POST'])
def join():
    # take info 
    data = request.json
    log.info(data)
    threading.Thread(target=reg_api, 
                     args=(data['con_str'], data['topic'], data['p_key'])).start()
    return str(blockChain.blocks)


# @app.route('/send_data', methods=['POST'])
# def send_post_data():
#     data = request.json
#     broadcast(data['payload'])
#     return True


# payload: {
#    filter  : <filter>,
#    sub_data: <data>
# }
@app.route('/send_data', methods=['POST'])
def send_get_data():
    # create block
    # send block
    data = request.json
    # bc = blockChain.get_instance()
    # zmq_obj = ZMQ_Soc.get_instance()
    fltr = data.get('filter')
    sub_data = data.get('subdata')
    broadcast_block('data', f'{fltr}|_|{sub_data}')
    return "Block added to broadcast queue"


@app.route('/cur_block', methods=['GET'])
def cur_block():
    bc = blockChain.get_instance()
    return str(len(bc.blocks))


@app.route('/subset', methods=['POST'])
def subset():
    bc = blockChain.get_instance()
    st_idx = int(data.get('start'))
    blocks = bc.blocks[st_idx:]
    return str(blocks)


@app.route('/peers', methods=['GET'])
def peers():
    return str(len(ZMQ_Soc.sub_list)) + json.dumps(str(ZMQ_Soc.sub_list))


@app.route('/ping', methods=['GET'])
def ping():
    return 'ping', 200


# p_key=<p_key>
@app.route('/stubs', methods=['POST'])
def stubs():
    data = request.get_json()
    log.info(f'stubs req: {data}')
    zmq = ZMQ_Soc.get_instance()
    bc = blockChain.get_instance()
    if zmq.find_in_list(data.get('p_key')):
        if data.get('p_key') not in bc.release_order:
            log.info('adding key to release')
            bc.release_order.append(data.get('p_key'))
            return json.dumps({}), 200
        else:
            return json.dumps({'error': 'already has a slot'}), 405
    else:
        return json.dumps({'error': 'peer not found'}), 406


def create_data(size_limit):
    s = ''.join([random.choice(string.ascii_letters) for i in range(size_limit)])
    return s


def send_data_req(data_tag):
    count = 0
    while True:
        time.sleep(60)
        print('about to send a request')
        data = f'{args.my_ip}-{count}'
        res = requests.post('http://localhost:9999/send_data', json={'filter': 'data', 'subdata': data + data_tag})
        print(res)
        count = count + 1


if __name__ == "__main__":
    data = create_data(args.data_size)
    bc = blockChain.get_instance()
    ZMQ_Soc.get_instance().add_subscription(args.my_ip, args.my_topic, bc.public_key)
    threading.Thread(target=proc_all, args=(bc.proc_queue,)).start()
    if args.node_ip:
        join_up(args.node_ip)
    else:
        first_man()
    thd_a = threading.Thread(target=send_data_req, args=(data,)).start()
    threading.Thread(target=sync_up, args=(args.delay * 3,)).start()
    thd = threading.Thread(target=main_listen_loop).start()
    app.run(host='0.0.0.0', port=9999, use_debugger=False)
