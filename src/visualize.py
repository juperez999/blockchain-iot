import matplotlib.pyplot as plt
import numpy as np

plt.rcParams.update({'font.size': 24})


round0 = [1.087, 1.087 , 1.087 , 1.091]
round05 = [1.192, 1.254, 1.78, 2.178]
round1 = [1.118, 2.439, 3.528, 3.61]

builds = np.array([4, 8, 12, 16])
y_stack = np.row_stack((round0, round05, round1)) 

fig = plt.figure(figsize=(11,8))
ax1 = fig.add_subplot(111)

ax1.plot(builds, y_stack[0,:], label='1kb', color='c', marker='o')
ax1.plot(builds, y_stack[1,:], label='500kb', color='g', marker='o')
ax1.plot(builds, y_stack[2,:], label='1Mb', color='r', marker='o')

plt.xticks(builds)
plt.xlabel('Number of Nodes')
plt.ylabel('Seconds per Block')
plt.title('Slimload Throughput Analysis')

handles, labels = ax1.get_legend_handles_labels()
lgd = ax1.legend(handles, labels, loc='upper left')
ax1.grid('on')

plt.savefig('throughput.png')

#mb
sizes0 = [0.941, 1.061, 1.200, 1.318]
sizes05 = [13.21, 21.13, 125.6, 130.4]
sizes1 = [92.93, 148.7, 147.6, 163.7]

builds = np.array([4, 8, 12, 16])
y_stack = np.row_stack((sizes0, sizes05, sizes1)) 

fig = plt.figure(figsize=(11,8))
ax1 = fig.add_subplot(111)

ax1.plot(builds, y_stack[0,:], label='1kb', color='c', marker='o')
ax1.plot(builds, y_stack[1,:], label='500kb', color='g', marker='o')
ax1.plot(builds, y_stack[2,:], label='1Mb', color='r', marker='o')

plt.xticks(builds)
plt.xlabel('Number of Nodes')
plt.ylabel('Blockchain Size (MB)')
plt.title('Slimload Storage Analysis')

handles, labels = ax1.get_legend_handles_labels()
lgd = ax1.legend(handles, labels, loc='upper left')
ax1.grid('on')

plt.savefig('size.png')


# test Successes/Failures

round0 = [5, 5, 5, 5]
round05 = [5, 3, 3, 2]
round1 = [5, 1, 1, 3]


builds = np.array([4, 8, 12, 16])
y_stack = np.row_stack((round0, round05, round1)) 

fig = plt.figure(figsize=(11,8))
ax1 = fig.add_subplot(111)

ax1.plot(builds, y_stack[0,:], label='1kb', color='c', marker='o')
ax1.plot(builds, y_stack[1,:], label='500kb', color='g', marker='o')
ax1.plot(builds, y_stack[2,:], label='1Mb', color='r', marker='o')

plt.xticks(builds)
plt.xlabel('Number of Nodes')
plt.ylabel('Full Runs')
plt.title('Slimload Robustness Analysis')

handles, labels = ax1.get_legend_handles_labels()
lgd = ax1.legend(handles, labels, loc='lower left')
ax1.grid('on')

plt.savefig('sync.png')