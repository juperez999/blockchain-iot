import boto3
import os
import paramiko
import socket
import time
import sys
from scp import SCPClient
import logging


log = logging.getLogger('')
log.setLevel(logging.INFO)

target_image_id = 'ami-028ac3f657aab9497'

def create_client(access_key, secret_key):
    client = boto3.Session(
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name='us-west-2'
    )
    ec2 = client.resource('ec2')
    return ec2


def make_start_vm_env(client, size=1, data_size=1, image_id=target_image_id):
    log.info('making vms...')
    userdata = f'#!/bin/sh\necho -e "\n"data_size={data_size * 1000000}>>"/home/ec2-user/blockchain_iot/block_args.py"'
    instances = client.create_instances(ImageId=image_id,
                                        MinCount=1,
                                        MaxCount=size,
                                        InstanceType='t2.micro',
                                        KeyName='block',
                                        SecurityGroups=['launch-wizard-4'],
                                        UserData=userdata)
    for instance in instances:
        log.info(instance)
    log.info('waiting on ips for vms...')
    instance = instances[0]
    while not instance.public_ip_address:
        instance = client.Instance(instance.id)
    log.info(f'got start node ip: {instance.public_ip_address}')
    time.sleep(20)
    return instance.public_ip_address, instance.id


def make_join_vm_env(client, ip, size=4, data_size=1, image_id=target_image_id):
    userdata = f'#!/bin/sh\necho -e node_ip="\'{ip}\'""\n"data_size={data_size * 500000}>>"/home/ec2-user/blockchain_iot/block_args.py"'
    log.info('making vms...')
    instances = client.create_instances(ImageId=image_id,
                                        MinCount=1,
                                        MaxCount=size,
                                        InstanceType='t2.micro',
                                        KeyName='block',
                                        SecurityGroups=['launch-wizard-4'],
                                        UserData=userdata)
    for instance in instances:
        log.info(instance)
    return instances


def create_ssh_client(ip, username='ec2-user', file_key='block.pem'):
    """ Creates a paramiko ssh client. """
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(ip, username=username, key_filename=file_key, timeout=30)
    except socket.timeout:
        raise Exception(
            "Took too long (>10s) to connect! Are you sure you're connected to Santa Clara VPN?"
        )
    return client


def scp_get(client, remote_path, local_path):
    """ Helper function over get command. """
    log.info("receiving...")
    bld_scp = SCPClient(client.get_transport())
    bld_scp.get(remote_path, local_path=local_path)
    bld_scp.close()
    return True


def anl_blocks(file_name):
    print(f'reading file {file_name}')
    fi_blocks = open(file_name, 'r')
    print('file read')
    blocks = []
    lines_read = fi_blocks.readlines()
    log.info(f'lines in file: {len(lines_read)}')
    for idx, line in enumerate(lines_read):
        if len(line) == 1:
            index = lines_read[idx + 1]
            time_stamp = lines_read[idx + 3]
            data = lines_read[idx + 4]
            blocks.append([index, time_stamp, data])
    log.info(f'blocks recovered: {len(blocks)}')
    data_blocks = []
    # grab all pertinent blocks
    log.info('getting all necessary blocks...')
    prev = float(blocks[0][1].split(':')[-1].split(';')[0])
    sum_up = 0.0
    for block in blocks[1:]:
        index = int(block[0].split(':')[-1].split(';')[0])
        time_st = float(block[1].split(':')[-1].split(';')[0])
        delta = time_st - prev
        prev = time_st
        if 'vote|_#_|closed|' in block[2]:
            delta = delta - 10
        log.info(f'{index} : {delta}')
        sum_up = sum_up + delta
    avg = sum_up / len(blocks)
    log.info(f'avg is {avg}')
    return avg
    # block_prev = 0
    # blocks_split = []
    # split = []
    # # split blocks into cycles - indexes
    # for block in data_blocks:
    #     target = int(block[0].split(':')[-1].split(';')[0])
    #     if int(block_prev) + 1 == target:
    #         split.append(block)
    #     else:
    #         if len(split) > 2:
    #             blocks_split.append(split)
    #         split = []
    #         split.append(block)
    #     block_prev = target
    # log.info(f'number of cycles: {len(blocks_split)}')
    # averages = []
    # # get delta averages per cycle - timestamp
    # log.info('getting delta averages...')
    # deltas = []
    # for block in blocks_split:
    #     log.info(f'new cycle {len(block)}')
    #     avg = 0.0
    #     prev = 0.0
    #     for blk in block:
    #         tar = float(blk[1].split(':')[-1].split(';')[0])
    #         if prev == 0.0:
    #             prev = tar
    #             log.info(f'set new prev: {prev}')
    #         else:
    #             delta = tar - prev
    #             deltas.append(delta)
    #             avg = avg + delta
    #             log.info(f'{tar} - {prev} = {delta}')
    #             prev = tar
    #     mean = avg / (len(block) - 1)
    #     log.info(f'cycle mean: {mean}')

    #     averages.append(mean)
    # print(f'number of deltas: {len(deltas)}')
    # # delta_mn = sum(float(i) for i in deltas) / len(deltas)
    # # log.info(f'delta mean: {delta_mn}')
    # all_avgs = 0.0
    # # calc the mean 
    # for avg in averages:
    #     all_avgs = all_avgs + avg
    # mean_avgs = all_avgs / len(averages)
    # log.info(f'full cycle mean data block broadcast: {mean_avgs}')



def start_log(size_data=1, node_size=5, run_time=600):
    log.handlers = []
    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    fileHandler = logging.FileHandler("{0}.log".format(f'{size_data}_{node_size}_{run_time}'))
    fileHandler.setFormatter(logFormatter)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    log.addHandler(consoleHandler)
    log.addHandler(fileHandler)


def main(size_data=1, node_size=5, run_time=600):
    start_log(size_data=size_data, run_time=rounds_secs, node_size=node_size)
    log.info(f'{size_data}, {run_time}')
    acc_key = os.environ.get('AWS_ACCESS_KEY_ID')
    sec_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
    ec2 = create_client(acc_key, sec_key)
    ip, m_id = make_start_vm_env(ec2, size=1, data_size=size_data)
    inst_ids = [m_id]
    new_vms = make_join_vm_env(ec2, ip, size=node_size, data_size=size_data)
    for vm in new_vms:
        inst_ids.append(vm.id)
    start = time.time()
    log.info(f'waiting for environment to set {start}')
    for i in range(run_time):
        time.sleep(1)
        print(f"\rcountdown: {run_time - i}", end="", flush=True)
    ssh_master = create_ssh_client(ip)
    log.info(time.time() - start)
    tar_file = f'blocks_{ip}-{size_data}mb.txt'
    stdin, stdout, stderr = ssh_master.exec_command('cp /home/ec2-user/blockchain_iot/blocks_pull.txt /home/ec2-user/blockchain_iot/blocks_cat.txt')
    for line in iter(stdout.readline, ""):
        log.info(line)
    for line in iter(stderr.readline, ""):
        log.info(line)
    stdin, stdout, stderr = ssh_master.exec_command('ls -l /home/ec2-user/blockchain_iot/blocks_cat.txt')
    for line in iter(stdout.readline, ""):
        log.info(line)
    scp_get(ssh_master, '/home/ec2-user/blockchain_iot/blockchain.log', f'blocks_{ip}.log')
    scp_get(ssh_master, '/home/ec2-user/blockchain_iot/blocks_cat.txt', tar_file)
    ec2.instances.filter(InstanceIds=inst_ids).terminate()
    return anl_blocks(tar_file)


start = time.time()
rounds_secs = int(sys.argv[1])
# nodes = int(sys.argv[2])
avgs = {}
for y in range(4, 15, 4):
    for x in range(1, 2):
        avgs[x] = []
        for i in range(5):
            try:

                avg = main(size_data=x, run_time=rounds_secs, node_size=y)
                log.info(f'round {i}: {avg}')
                avgs[x].append(avg)
            except Exception as e:
                log.error(str(e))
            time.sleep(20)
    for key, val in avgs.items():
        grp_avg = sum(val)/len(val)
        log.info(f'final avg {y} n for {key}: {grp_avg}')
end = time.time() - start
log.info(f'full time duration: {end} seconds')
# main(size_data=4, run_time=rounds_secs, node_size=nodes)