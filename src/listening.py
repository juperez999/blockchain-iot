from state_machine import ZMQ_Soc, BlockQueue
import logging
import random
import time
import requests
from blockchain import blockChain
import block_args as args
import json
import threading

import multiprocessing
import os
import random


log = logging.getLogger()


def join_up(node_ip):
    bc = blockChain.get_instance()
    endpoint = f'http://{node_ip}:9999/join'
    payload = {
        'con_str': args.my_ip,
        'topic': args.my_topic,
        'p_key': bc.get_key_pair()
    }
    res = requests.post(endpoint,
                        headers={'Content-Type': 'application/json'},
                        data=json.dumps(payload))
    # log.info(res.text)
    bc.extract_list_blocks(res.text)


def first_man():
    bc = blockChain.get_instance()
    blk2 = bc.gen_block('register', f'hello|_|{args.my_topic}_|_{args.my_ip}_|_{bc.get_key_pair()}')
    log.info('registering')
    if bc.verify_block(blk2):
        bc.consume(blk2)
    blk3 = bc.gen_block('oracle', f'new|_|{bc.public_key}')
    if bc.verify_block(blk3):
        bc.consume(blk3)


def get_index_release():
    bc = blockChain.get_instance()
    if not bc.vote_live and int(bc.current_vote) > 0:
        for index, entry in enumerate(bc.release_order):
            if entry == bc.public_key:
                return index + bc.current_block + 1
    return False


def my_block_next():
    bc = blockChain.get_instance()
    log.info(f'chekcing next block {bc.current_block} {bc.broad_block_num}')
    if int(bc.current_block) == int(bc.broad_block_num):
        # it is my turn broadcast it
        return True
    return False


def i_am_oracle():
    bc = blockChain.get_instance()
    if bc.current_oracle == bc.public_key:
        return True
    return False


def all_prev_vote_blocks_recvd():
    bc = blockChain.get_instance()
    log.info(f'blocks check, {bc.current_block} {bc.current_vote_start} {len(bc.release_order)}')
    # andle initial blocks, no votes yet
    check_count = int(bc.current_vote_start) + 2
    if len(bc.release_order) > 0:
        log.info('releases exist, adding to increment')
        check_count = int(bc.current_vote_start) + 2 + len(bc.release_order) + 1
    if not bc.vote_live and bc.res_out and bc.current_vote > 0 and int(bc.current_block) < check_count:
        print('still missing blocks')
        return False
    print('I have all previous blocks')
    return True


def ping_out(tar_ip):
    cur_idx = get_peer_cur(tar_ip)
    if cur_idx == len(blockChain.get_instance().blocks):
        return True
    return False


def pick_new_oracle():
    log.info('picking new oracle')
    zmq = ZMQ_Soc.get_instance()
    bc = blockChain.get_instance()
    res = get_from_peers_list()
    log.info('new oracle found')
    block = bc.gen_block('oracle', f'new|_|{res.get("p_key")}')
    zmq.broadcast(str(block))



def oracle_action():
    bc = blockChain.get_instance()
    log.info(f'oracle: {bc.prev_vote} {bc.current_vote} {bc.vote_live} {bc.oracle_move} {bc.res_out} {bc.vote_closed}')
    if not bc.vote_live and not bc.oracle_move and not bc.res_out and not bc.vote_closed and bc.oracle_changed and (int(bc.current_vote) == int(bc.prev_vote)):
        log.info('in vote not live')
        bc.vote_status('open')
        bc.oracle_changed = False
        bc.oracle_move = True
        # create vote open block with my ip and min open time
        # as oracle wait for min open time to finish
        # send close_vote
        # time.sleep(5)
        # bc.vote_status('closed')
        # consume all stubs, create release order
        # send release_order   
        return
    elif not bc.vote_live and bc.oracle_move and not bc.res_out and bc.vote_closed and (int(bc.current_vote) > int(bc.prev_vote)):
        log.info(f'in vote broadcast {bc.release_order}')
        if bc.release_order:
            log.info('in release order')
            random.shuffle(bc.release_order)
            bc.broad_results(bc.release_order)
        else:
            log.info('in list only')
            bc.broad_results([])
        bc.res_out = True
        bc.oracle_move = False
        return
    elif bc.res_out:
        log.info('res out true')
        if all_prev_vote_blocks_recvd():
            log.info('in vote oracle')
            # choose new oracle (New Oracle is XXX.XXX.XXX.XXX)
            # needs to happen after
            pick_new_oracle()
            bc.res_out = False
            bc.oracle_move = False
            bc.vote_closed = False
            bc.vote_live = False
            bc.release_order = []
            bc.prev_vote = bc.current_vote
        return


def next_move():
    # grab state machine, and blockchain
    log.info('in next move')
    bc = blockChain.get_instance()
    if my_block_next():
        log.info('Its my turn, sending...')
        bc.broad_block_queue()
    elif i_am_oracle() and all_prev_vote_blocks_recvd():
        log.info('Oracle action check')
        oracle_action()


def proc_all(qu):
    while True:
        bc = blockChain.get_instance()
        #zmq_obj = ZMQ_Soc.get_instance()
        block = qu.get()
        blk = bc.extract_block(block)
        if bc.verify_block(blk) and bc.consume(blk):
            statement = f'consuming block {blk.index}'
            log.info(statement)
            # echo block
            # log.info('about to echo')
            #zmq_obj.broadcast(block)
            log.info('making next move')
            next_move()
        else:
             log.error('block dropped: %s', blk.index)


def timeout_check(delay):
    time.sleep(delay)
    time.sleep(random.randrange(delay))
    pick_new_oracle()


def get_from_peers_list():
    zmq = ZMQ_Soc.get_instance()
    res = False
    while(not res):
        res = random.choice(zmq.sub_list)
        if len(zmq.sub_list) > 1:
            res_ip = res.get('connect')
            if args.my_ip not in res_ip:
                if ping_out(res_ip):
                    log.info('got peer ' + res_ip)
                else:
                    res = None
                    log.info('peer out of sync ' + res_ip)
            else:
                log.info('bad oracle selection')
                res = None
    return res


def sync_up(delay):
    while True:
        time.sleep(delay)
        log.info('running sync_up')
        bc = blockChain.get_instance()
        sync_dict = {}
        # do 2 times:
        zmq = ZMQ_Soc.get_instance()
        i = 0
        while len(sync_dict) < 1 and len(zmq.sub_list) > 1 and i < 5:
            i = i + 1
            peer = get_from_peers_list()
            # pick a peer
            tar_ip = peer.get('connect')
            cur_idx = get_peer_cur(tar_ip)
            if cur_idx > len(bc.blocks):
                log.info('found a peer with more blocks')
                sync_dict[tar_ip] = cur_idx
                # get cur {dictionary <ip>: <current index>}
                log.info('getting the next block')
                # all the same values in both peers 
                tar_ip = sync_dict.keys()[0]
                get_subset_peer(tar_ip)
        # set the list of currents, len == 1 only, should be the same
        # pick one of the peers
        # get_subset from peer
        # put subset in processing queue


def get_peer_cur(target_ip):
    endpoint = f'http://{target_ip}:9999/cur_block'
    res = requests.get(endpoint)
    return int(res.text)


def get_subset_peer(target_ip):
    bc = blockChain.get_instance()
    endpoint = f'http://{target_ip}:9999/subset'
    payload = {
        'start': str(len(bc.blocks) + 1)
    }
    res = requests.post(endpoint,
                        headers={'Content-Type': 'application/json'},
                        data=json.dumps(payload))
    bc.extract_list_blocks(res.text)


def main_listen_loop():
    bc = blockChain.get_instance()
    log.info("listening")
    log.info('blockchain in')
    zmq_obj = ZMQ_Soc.get_instance()
    log.info('zmq instance in')
    socket = zmq_obj.get_sub_sock()
    log.info('socket grabbed')
    while True:
        messagedata = str(socket.recv())
        # split and process message data, topic
        topic, block = messagedata.split('#_|_#')
        # int(blk.index) == len(bc.blocks) + proc_queue.qsize() and 
        log.info('processing block')
        bc.proc_queue.put(block)
